package com.meta.onlineshopping.model;

import com.meta.onlineshopping.enums.Status;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "online_order")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
    @JoinColumn(name = "user_id", nullable = false, foreignKey = @ForeignKey(name = "fk_onlineorder_user"))
    private User user;

    @Column(name = "order_completion_date")
    private LocalDateTime orderCompletionDate;

    @Column(name = "delivery_date_time")
    private LocalDateTime deliveryDateTime;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "order_product_id_map", joinColumns = @JoinColumn(name = "online_order_id"))
    private List<Integer> productIds;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Column(name = "total_cost")
    private Double totalCost;
}
