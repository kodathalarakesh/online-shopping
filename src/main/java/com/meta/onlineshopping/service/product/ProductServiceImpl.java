package com.meta.onlineshopping.service.product;

import com.meta.onlineshopping.dto.product.ProductRequestDto;
import com.meta.onlineshopping.dto.product.ProductResponseDto;
import com.meta.onlineshopping.exceptions.DataNotFoundException;
import com.meta.onlineshopping.exceptions.ServiceValidationException;
import com.meta.onlineshopping.model.Category;
import com.meta.onlineshopping.model.Product;
import com.meta.onlineshopping.model.User;
import com.meta.onlineshopping.repository.CategoryRepo;
import com.meta.onlineshopping.repository.ProductRepo;
import com.meta.onlineshopping.repository.UserRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepo productRepo;
    private final UserRepo userRepo;
    private final CategoryRepo categoryRepo;

    public ProductServiceImpl(ProductRepo productRepo, UserRepo userRepo, CategoryRepo categoryRepo) {
        this.productRepo = productRepo;
        this.userRepo = userRepo;
        this.categoryRepo = categoryRepo;
    }

    @Override
    public void create(ProductRequestDto productRequestDto) {
        User user = userRepo.findById(productRequestDto.getUserId()).orElseThrow(
                () -> new DataNotFoundException("User with id:" + productRequestDto.getUserId() + " is not present !!")
        );

        List<Category> categories = categoryRepo.findAllByIds(productRequestDto.getCategoryIds());
        if (categories.size() != productRequestDto.getCategoryIds().size()) {
            throw new ServiceValidationException("Error getting category form the list of category id!! Please provide correct ids.");
        }

        Product product = new Product(productRequestDto);
        product.setUser(user);
        product.setCategoryList(categories);

        productRepo.save(product);
    }

    @Override
    public void update(ProductRequestDto productRequestDto) {
        Product product = productRepo.findById(productRequestDto.getId()).orElseThrow(
                () -> new DataNotFoundException("Product with id:" + productRequestDto.getId() + " is not present !!")
        );

        List<Category> categories = categoryRepo.findAllByIds(productRequestDto.getCategoryIds());
        if (categories.size() != productRequestDto.getCategoryIds().size()) {
            throw new ServiceValidationException("Error getting category form the list of category id!! Please provide correct ids.");
        }

        product.setProductName(productRequestDto.getProductName());
        product.setDescription(productRequestDto.getDescription());
        product.setPrice(productRequestDto.getPrice());
        product.setQuantity(productRequestDto.getQuantity());
        product.setCategoryList(categories);

        productRepo.save(product);

    }

    @Override
    public ProductResponseDto findById(Integer id) {
        Product product = productRepo.findById(id).orElseThrow(
                () -> new DataNotFoundException("Product with id:" + id + " is not present !!")
        );
        return new ProductResponseDto(product);
    }

    @Override
    public List<ProductResponseDto> findAll() {
        List<Product> productList = productRepo.findAll();
        return productList.stream().map(p -> new ProductResponseDto(p)).collect(Collectors.toList());
    }

    @Override
    public void deleteById(Integer id) {
        Product product = productRepo.findById(id).orElseThrow(
                () -> new DataNotFoundException("Product with id:" + id + " is not present !!")
        );
        productRepo.delete(product);
    }
}
