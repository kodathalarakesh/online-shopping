package com.meta.onlineshopping.service.product;

import com.meta.onlineshopping.dto.product.ProductRequestDto;
import com.meta.onlineshopping.dto.product.ProductResponseDto;

import java.util.List;

public interface ProductService {

    void create(ProductRequestDto productRequestDto);

    void update(ProductRequestDto productRequestDto);

    ProductResponseDto findById(Integer id);

    List<ProductResponseDto> findAll();

    void deleteById(Integer id);
}
