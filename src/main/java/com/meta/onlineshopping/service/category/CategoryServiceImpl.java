package com.meta.onlineshopping.service.category;

import com.meta.onlineshopping.dto.category.CategoryDto;
import com.meta.onlineshopping.exceptions.DataNotFoundException;
import com.meta.onlineshopping.model.Category;
import com.meta.onlineshopping.repository.CategoryRepo;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepo categoryRepo;

    @Override
    public void create(CategoryDto categoryDto) {
        Category category = new Category();
        category.setDescription(categoryDto.getDescription());
        category.setCode(categoryDto.getCode());
        category.setCategoryName(categoryDto.getCategoryName());
        category.setActive(true);
        categoryRepo.save(category);
    }

    @Override
    public void update(CategoryDto categoryDto) {
        Category category = categoryRepo.findById(categoryDto.getId()).orElseThrow(
                () -> new DataNotFoundException("Category with id:" + categoryDto.getId() + " is not present !!")
        );
        category.setDescription(categoryDto.getDescription());
        category.setCode(categoryDto.getCode());
        category.setCategoryName(categoryDto.getCategoryName());
        categoryRepo.save(category);
    }

    @Override
    public CategoryDto findById(Integer id) {
        Category category = categoryRepo.findById(id).orElseThrow(
                () -> new DataNotFoundException("Category with id:" + id + " is not present !!")
        );
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(category.getId());
        categoryDto.setCategoryName(category.getCategoryName());
        categoryDto.setDescription(category.getDescription());
        categoryDto.setActive(category.isActive());
        return categoryDto;
    }

    @Override
    public List<CategoryDto> findAll() {
        List<Category> categories = categoryRepo.findAll();
        return categories.stream().map(c -> new CategoryDto(c)).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void toggle(Integer id) {
        categoryRepo.toggleById(id);
    }
}
