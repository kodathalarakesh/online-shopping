package com.meta.onlineshopping.service.category;

import com.meta.onlineshopping.dto.category.CategoryDto;

import java.util.List;

public interface CategoryService {

    void create(CategoryDto categoryDto);

    void update(CategoryDto categoryDto);
    CategoryDto findById(Integer id);
    List<CategoryDto> findAll();
    void toggle(Integer id);
}
