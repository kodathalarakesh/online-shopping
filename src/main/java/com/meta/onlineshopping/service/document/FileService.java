package com.meta.onlineshopping.service.document;

import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface FileService {

    String uploadProfileImage(MultipartFile multipartFile, Integer userId) throws IOException;


    void downloadProfileImage(String fileName, HttpServletResponse response) throws FileNotFoundException;
}
