package com.meta.onlineshopping.service.document;

import com.meta.onlineshopping.exceptions.DataNotFoundException;
import com.meta.onlineshopping.model.User;
import com.meta.onlineshopping.repository.UserRepo;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class FileServiceImpl implements FileService {

    private final UserRepo userRepo;

    public FileServiceImpl(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public String uploadProfileImage(MultipartFile multipartFile, Integer userId) throws IOException {
        User user = userRepo.findById(userId).orElseThrow(
                () -> new DataNotFoundException("User with id:" + userId + " not found !!")
        );

        String directory = System.getProperty("user.home") + "/" + "Online Shopping Documents";
        File file = new File(directory);
        if (!file.exists()) {
            file.mkdir();
        }

        String originalFileName = multipartFile.getOriginalFilename();

        //generate UUID
        String uuid = UUID.randomUUID().toString();

        String newFileName = uuid.concat(originalFileName.substring(originalFileName.lastIndexOf('.')));
        String newPath = directory + "/" + newFileName;

        Files.copy(multipartFile.getInputStream(), Paths.get(newPath));

        user.setFileName(newFileName);
        user.setOriginalName(originalFileName);
        user.setFilePath(newPath);
        userRepo.save(user);

        return newPath;
    }

    @Override
    public void downloadProfileImage(String fileName, HttpServletResponse response) throws FileNotFoundException {
        User user = userRepo.findByFileName(fileName).orElseThrow(
                () -> new DataNotFoundException("Profile image could not be found !!")
        );
        File file = new File(user.getFilePath());
        if (file.exists()) {
            //mimeType  --> is the type of the file. For example : images/jpg  application/pdf
            String mimeType = URLConnection.guessContentTypeFromName(file.getName());
            if (mimeType == null) mimeType = "application/octet-stream";

            response.setContentType(mimeType);
            response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + user.getOriginalName() + "\"");
            response.setContentLength((int) file.length());

            try {
                InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
                FileCopyUtils.copy(inputStream, response.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            throw new FileNotFoundException("File Not found in server !!");
        }
    }
}
