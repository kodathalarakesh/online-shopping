package com.meta.onlineshopping.service.user;

import com.meta.onlineshopping.dto.user.UserListResponseDto;
import com.meta.onlineshopping.dto.user.UserRequestDto;
import com.meta.onlineshopping.dto.user.UserResponseDto;

import java.util.List;

public interface UserService {

    //CRUD operation: CREATE , READ , UPDATE and DELETE

    void create(UserRequestDto userRequestDto);

    void update(UserRequestDto userRequestDto);

    UserResponseDto findById(Integer userId);

    List<UserListResponseDto> findAll();

    void deleteById(Integer userId);
}
