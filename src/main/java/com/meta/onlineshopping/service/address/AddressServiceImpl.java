package com.meta.onlineshopping.service.address;

import com.meta.onlineshopping.dto.address.AddressRequestDto;
import com.meta.onlineshopping.dto.address.AddressResponseDto;
import com.meta.onlineshopping.exceptions.DataNotFoundException;
import com.meta.onlineshopping.model.Address;
import com.meta.onlineshopping.model.User;
import com.meta.onlineshopping.repository.AddressRepo;
import com.meta.onlineshopping.repository.UserRepo;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {
    private final AddressRepo addressRepo;
    private final UserRepo userRepo;

    public AddressServiceImpl(AddressRepo addressRepo, UserRepo userRepo) {
        this.addressRepo = addressRepo;
        this.userRepo = userRepo;
    }

    @Override
    @Transactional
    public void create(AddressRequestDto addressRequestDto) {
        Address address = new Address();

        User user = userRepo.findById(addressRequestDto.getUserId()).orElseThrow(
                () -> new DataNotFoundException("User with id:" + addressRequestDto.getUserId() + " is not present")
        );

        address.setCountry(addressRequestDto.getCountry());
        address.setState(addressRequestDto.getState());
        address.setStreet(addressRequestDto.getStreet());
        address.setHouseNo(addressRequestDto.getHouseNo());
        address.setZipCode(addressRequestDto.getZipCode());
        address.setUser(user);

        addressRepo.save(address);
    }

    @Override
    @Transactional
    public void update(AddressRequestDto addressRequestDto) {
        Address address = addressRepo.findById(addressRequestDto.getId()).orElseThrow(
                () -> new DataNotFoundException("Address with id:" + addressRequestDto.getId() + " is not present!!")
        );
        address.setCountry(addressRequestDto.getCountry());
        address.setState(addressRequestDto.getState());
        address.setStreet(addressRequestDto.getStreet());
        address.setHouseNo(addressRequestDto.getHouseNo());
        address.setZipCode(addressRequestDto.getZipCode());
        addressRepo.save(address);
    }

    @Override
    public AddressResponseDto findById(Integer id) {
        Address address = addressRepo.findById(id).orElseThrow(
                () -> new DataNotFoundException("Address with id:" + id + " is not present!!")
        );
        AddressResponseDto addressResponseDto = new AddressResponseDto();
        addressResponseDto.setId(address.getId());
        String fullAddress = new StringBuilder(address.getCountry()).append(", ").append(address.getState()).append(", ").append(address.getStreet()).append("-").append(address.getHouseNo()).toString();
        addressResponseDto.setFullAddress(fullAddress);
        addressResponseDto.setUserId(address.getUser().getId());
        addressResponseDto.setZipCode(address.getZipCode());
        addressResponseDto.setUsername(address.getUser().getUsername());
        return addressResponseDto;
    }

    @Override
    public List<AddressResponseDto> findAll() {
        List<Address> addressList = addressRepo.findAll();
        List<AddressResponseDto> addressResponseDtos = new ArrayList<>();
        for (Address address : addressList) {
            AddressResponseDto addressResponseDto = new AddressResponseDto();
            addressResponseDto.setId(address.getId());
            String fullAddress = new StringBuilder(address.getCountry()).append(", ").append(address.getState()).append(", ").append(address.getStreet()).append("-").append(address.getHouseNo()).toString();
            addressResponseDto.setFullAddress(fullAddress);
            addressResponseDto.setZipCode(address.getZipCode());
            addressResponseDto.setUserId(address.getUser().getId());
            addressResponseDto.setUsername(address.getUser().getUsername());
            addressResponseDtos.add(addressResponseDto);
        }
        return addressResponseDtos;
    }

    @Override
    public void deleteById(Integer id) {
        Address address = addressRepo.findById(id).orElseThrow(
                () -> new DataNotFoundException("Address with id:" + id + " is not present!!")
        );
        addressRepo.delete(address);
    }

    @Override
    public AddressResponseDto findByUserId(Integer userId) {
        Address address = addressRepo.findByUserId(userId).orElseThrow(
                () -> new DataNotFoundException("Address of user with userId:" + userId + " id not present !!")
        );
        AddressResponseDto addressResponseDto = new AddressResponseDto();
        addressResponseDto.setId(address.getId());
        String fullAddress = new StringBuilder(address.getCountry()).append(", ").append(address.getState()).append(", ").append(address.getStreet()).append("-").append(address.getHouseNo()).toString();
        addressResponseDto.setFullAddress(fullAddress);
        addressResponseDto.setZipCode(address.getZipCode());
        addressResponseDto.setUserId(address.getUser().getId());
        addressResponseDto.setUsername(address.getUser().getUsername());
        return addressResponseDto;
    }
}
