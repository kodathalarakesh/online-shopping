package com.meta.onlineshopping.service.address;

import com.meta.onlineshopping.dto.address.AddressRequestDto;
import com.meta.onlineshopping.dto.address.AddressResponseDto;

import java.util.List;

public interface AddressService {

    void create(AddressRequestDto addressRequestDto);

    void update(AddressRequestDto addressRequestDto);

    AddressResponseDto findById(Integer id);

    List<AddressResponseDto> findAll();

    void deleteById(Integer id);

    AddressResponseDto findByUserId(Integer userId);
}
