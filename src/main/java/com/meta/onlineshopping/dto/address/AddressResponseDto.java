package com.meta.onlineshopping.dto.address;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressResponseDto {

    private Integer id;
    private String fullAddress;

    private String zipCode;

    private Integer userId;

    private String username;
}
