package com.meta.onlineshopping.dto.address;

import com.meta.onlineshopping.model.User;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressRequestDto {
    private Integer id;

    @NotNull(message = "country cannot be null !!")
    private String country;

    @NotNull(message = "state cannot be null !!")
    private String state;

    @NotNull(message = "street cannot be null!!")
    private String street;

    @NotNull(message = "house no cannot be null !!")
    private String houseNo;

    @NotNull(message = "zip code cannot be null")
    private String zipCode;

    @NotNull(message = "user id cannot be null")
    private Integer userId;
}
