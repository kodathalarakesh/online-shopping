package com.meta.onlineshopping.dto.user;

import com.meta.onlineshopping.enums.Gender;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserRequestDto {

    private Integer id;

    @NotNull(message = "User's username cannot be null")
    @NotBlank(message = "User's username cannot be blank")
    private String username;

    @NotNull(message = "User's full name cannot be null")
    @NotBlank(message = "User's full name cannot be blank")
    private String userFullName;

    @Pattern(regexp = "^[0-9]{10}$",message = "Mobile Number should be 10 digits only")
    private String userMobileNumber;

    @Size(min = 8, max = 24, message = "Password should have length between 8 and 24")
    private String password;

    @Email(message = "Please enter a valid email address")
    private String email;

    @NotNull(message = "User's gender cannot be null")
    private Gender gender;

    @NotEmpty(message = "hobbies cannot be empty")
    private List<String> hobbies;

    @Min(value = 17, message = "Minimum age should be 17")
    @Max(value = 80, message = "Maximum age should be 80")
    private Integer age;


}
