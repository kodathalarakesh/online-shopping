package com.meta.onlineshopping.dto.user;

import com.meta.onlineshopping.enums.Gender;
import com.meta.onlineshopping.enums.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseDto {

    private Integer id;

    private String username;

    private String userFullName;

    private String userMobileNumber;

    private String email;

    private Gender gender;

    private Role role;

    private boolean isActive;
}
