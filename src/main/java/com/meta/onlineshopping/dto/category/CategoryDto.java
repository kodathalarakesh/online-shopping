package com.meta.onlineshopping.dto.category;

import com.meta.onlineshopping.model.Category;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryDto {

    private Integer id;

    @NotNull(message = "category message cannot be null !!")
    @NotBlank(message = "category message cannot be blank !!")
    private String categoryName;

    @NotNull(message = "description message cannot be null !!")
    @NotBlank(message = "description message cannot be blank !!")
    private String description;

    @NotNull(message = "code message cannot be null !!")
    @NotBlank(message = "code message cannot be blank !!")
    private String code;

    private boolean isActive;

    public CategoryDto(Category category) {
        this.id = category.getId();
        this.categoryName = category.getCategoryName();
        this.description = category.getDescription();
        this.code = category.getCode();
        this.isActive = category.isActive();
    }
}
