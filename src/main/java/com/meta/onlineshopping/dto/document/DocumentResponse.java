package com.meta.onlineshopping.dto.document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DocumentResponse {

    private Integer id;

    private String fileName;

    private String filePath;

    private String originalFileName;
}
