package com.meta.onlineshopping.dto.product;

import com.meta.onlineshopping.dto.category.CategoryDto;
import com.meta.onlineshopping.model.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponseDto {

    private Integer id;

    private String productName;

    private String description;

    private Double price;

    private Integer quantity;

    private Integer userId;
    private String userName;
    private List<CategoryDto> categoryDtoList;

    public ProductResponseDto(Product product) {
        this.id = product.getId();
        this.productName = product.getProductName();
        this.price = product.getPrice();
        this.quantity = product.getQuantity();
        this.description = product.getDescription();
        this.userId = product.getUser().getId();
        this.userName = product.getUser().getFullName();
        this.categoryDtoList = product.getCategoryList().stream().map(c -> new CategoryDto(c)).collect(Collectors.toList());
    }
}
