package com.meta.onlineshopping.dto.product;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequestDto {
    private Integer id;

    @NotNull(message = "productName cannot be null !!")
    @NotBlank(message = "productName cannot be blank !!")
    private String productName;

    @NotNull(message = "description cannot be null !!")
    @NotBlank(message = "description cannot be blank !!")
    private String description;

    @NotNull(message = "price cannot be null !!")
    private Double price;

    @NotNull(message = "quantity cannot be null !!")
    private Integer quantity;

    @NotNull(message = "userId cannot be null !!")
    private Integer userId;
    @NotEmpty(message = "categoryIds cannot be empty !!")
    private List<Integer> categoryIds;

}
