package com.meta.onlineshopping.controller;

import com.meta.onlineshopping.dto.category.CategoryDto;
import com.meta.onlineshopping.dto.global.GlobalApiResponse;
import com.meta.onlineshopping.service.category.CategoryService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/category")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;

    @PostMapping("/save")
    public ResponseEntity<GlobalApiResponse> createCategory(@Valid @RequestBody CategoryDto categoryDto) {
        categoryService.create(categoryDto);
        return new ResponseEntity<>(new GlobalApiResponse("Category created successfully", true, null), HttpStatus.OK);

    }

    @PutMapping("/update")
    public ResponseEntity<GlobalApiResponse> updateCategory(@Valid @RequestBody CategoryDto categoryDto) {
        categoryService.update(categoryDto);
        return new ResponseEntity<>(new GlobalApiResponse("Category updated successfully", true, null), HttpStatus.OK);

    }

    @GetMapping("/id/{categoryId}")
    public ResponseEntity<GlobalApiResponse> getById(@PathVariable("categoryId") Integer categoryId) {
        return new ResponseEntity<>(new GlobalApiResponse("Category fetched successfully", true, categoryService.findById(categoryId)), HttpStatus.OK);

    }

    @GetMapping
    public ResponseEntity<GlobalApiResponse> getAll() {
        return new ResponseEntity<>(new GlobalApiResponse("Category list fetched successfully", true, categoryService.findAll()), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<GlobalApiResponse> toggleById(@PathVariable(value = "id") Integer id) {
        categoryService.toggle(id);
        return new ResponseEntity<>(new GlobalApiResponse("Category toggle successfully", true, null), HttpStatus.OK);
    }
}
