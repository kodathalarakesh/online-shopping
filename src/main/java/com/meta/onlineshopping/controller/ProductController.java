package com.meta.onlineshopping.controller;

import com.meta.onlineshopping.dto.global.GlobalApiResponse;
import com.meta.onlineshopping.dto.product.ProductRequestDto;
import com.meta.onlineshopping.service.product.ProductService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/product")
public class ProductController {
    private final ProductService productService;

    @PostMapping("/save")
    public ResponseEntity<GlobalApiResponse> save(@Valid @RequestBody ProductRequestDto productRequestDto) {
        productService.create(productRequestDto);
        return new ResponseEntity<>(new GlobalApiResponse("Product created successfully", true, null), HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<GlobalApiResponse> update(@Valid @RequestBody ProductRequestDto productRequestDto) {
        productService.update(productRequestDto);
        return new ResponseEntity<>(new GlobalApiResponse("Product updated successfully", true, null), HttpStatus.OK);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<GlobalApiResponse> getById(@PathVariable Integer id) {
        return new ResponseEntity<>(new GlobalApiResponse("Product fetched successfully", true, productService.findById(id)), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<GlobalApiResponse> getAll() {
        return new ResponseEntity<>(new GlobalApiResponse("Product fetched successfully", true, productService.findAll()), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<GlobalApiResponse> delete(@PathVariable Integer id) {
        productService.deleteById(id);
        return new ResponseEntity<>(new GlobalApiResponse("Product deleted successfully", true, null), HttpStatus.OK);
    }

}
