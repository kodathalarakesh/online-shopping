package com.meta.onlineshopping.controller;

import com.meta.onlineshopping.dto.global.GlobalApiResponse;
import com.meta.onlineshopping.service.document.FileService;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.digester.Digester;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/document")
public class FileController {

    private final FileService fileService;

    @PostMapping("/upload/user-profile")
    public ResponseEntity<GlobalApiResponse> uploadProfileImage(@RequestParam("file") MultipartFile file, @RequestParam("userId") Integer userId) {
        String filePath = null;
        try {
            filePath = fileService.uploadProfileImage(file, userId);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(new GlobalApiResponse("File could not be uploaded as File not found !!", false, null), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new GlobalApiResponse("File Uploaded successfully", true, filePath), HttpStatus.OK);
    }

    @GetMapping("/download")
    public void downloadProfileImage(@RequestParam("fileName") String fileName, HttpServletResponse response) {
        try {
            fileService.downloadProfileImage(fileName, response);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }
}
