package com.meta.onlineshopping.controller;

import com.meta.onlineshopping.dto.address.AddressRequestDto;
import com.meta.onlineshopping.dto.global.GlobalApiResponse;
import com.meta.onlineshopping.service.address.AddressService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/addresses")
@RequiredArgsConstructor
public class AddressController {

    private final AddressService addressService;

    @PostMapping("/save")
    public ResponseEntity<GlobalApiResponse> createAddress(@Valid @RequestBody AddressRequestDto addressRequestDto) {
        addressService.create(addressRequestDto);
        return new ResponseEntity<>(new GlobalApiResponse("Address created successfully", true, null), HttpStatus.OK);

    }


    @PutMapping("/update")
    public ResponseEntity<GlobalApiResponse> updateAddress(@Valid @RequestBody AddressRequestDto addressRequestDto) {
        addressService.update(addressRequestDto);
        return new ResponseEntity<>(new GlobalApiResponse("Address updated successfully", true, null), HttpStatus.OK);

    }

    @GetMapping("/id/{addressId}")
    public ResponseEntity<GlobalApiResponse> getById(@PathVariable("addressId") Integer addressId) {
        return new ResponseEntity<>(new GlobalApiResponse("Address fetched successfully", true, addressService.findById(addressId)), HttpStatus.OK);

    }

    @GetMapping("/find-by-user-id/{userId}")
    public ResponseEntity<GlobalApiResponse> getByUserId(@PathVariable("userId") Integer userId) {
        return new ResponseEntity<>(new GlobalApiResponse("Address fetched successfully", true, addressService.findByUserId(userId)), HttpStatus.OK);

    }

    @GetMapping
    public ResponseEntity<GlobalApiResponse> getAll() {
        return new ResponseEntity<>(new GlobalApiResponse("Address list fetched successfully", true, addressService.findAll()), HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<GlobalApiResponse> deleteById(@RequestParam(value = "id") Integer id) {
        addressService.deleteById(id);
        return new ResponseEntity<>(new GlobalApiResponse("Address deleted successfully", true, null), HttpStatus.OK);
    }

}
