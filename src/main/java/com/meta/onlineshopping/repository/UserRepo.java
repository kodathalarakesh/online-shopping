package com.meta.onlineshopping.repository;

import com.meta.onlineshopping.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User, Integer> {

    @Query(value = "select * from auth_user where file_name=?1", nativeQuery = true)
    Optional<User> findByFileName(String fileName);

    Optional<User> findByEmail(String email);
}
