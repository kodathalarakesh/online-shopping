package com.meta.onlineshopping.repository;

import com.meta.onlineshopping.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepo extends JpaRepository<Category, Integer> {

    @Modifying
    @Query(value = "update category set is_active = NOT  is_active where id=?1", nativeQuery = true)
    void toggleById(Integer id);


    @Query(value = "select * from category where id in ?1 and is_active", nativeQuery = true)
    List<Category> findAllByIds(List<Integer> ids);
}
