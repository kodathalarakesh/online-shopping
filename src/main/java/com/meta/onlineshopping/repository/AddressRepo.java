package com.meta.onlineshopping.repository;

import com.meta.onlineshopping.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AddressRepo extends JpaRepository<Address, Integer> {

    @Query("select a from Address a where a.user.id=?1 ")
    Optional<Address> findByUserId(Integer userId);

}
