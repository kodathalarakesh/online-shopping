package com.meta.onlineshopping.enums;

import lombok.Getter;

@Getter
public enum Role {
    ADMIN, SELLER, BUYER;
}
