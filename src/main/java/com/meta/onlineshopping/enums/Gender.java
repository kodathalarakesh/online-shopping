package com.meta.onlineshopping.enums;

import lombok.Getter;

@Getter
public enum Gender {

    MALE, FEMALE, OTHER;
}
